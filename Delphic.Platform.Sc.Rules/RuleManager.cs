﻿using System;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Rules;
using Sitecore.SecurityModel;

namespace Delphic.Platform.Sc.Rules
{
    /// <summary>
    /// Class for running rules
    /// </summary>
    /// <remarks>
    /// Credit to Jim "Jimbo" Baltika as the original developer of this RuleManager class
    /// </remarks>
    public static class RuleManager
    {
        /// <summary>
        /// The run rules.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="rulesFolder">The rules folder.</param>
        public static void RunRules([NotNull] Item item, [NotNull] ID rulesFolder)
        {
            Assert.ArgumentNotNull(item, nameof(item));
            Assert.ArgumentNotNull(rulesFolder, nameof(rulesFolder));
            try
            {
                if (!Settings.Rules.ItemEventHandlers.RulesSupported(item.Database))
                {
                    return;
                }

                Item rulesFolderItem;
                using (new SecurityDisabler())
                {
                    rulesFolderItem = item.Database.GetItem(rulesFolder);
                    if (rulesFolderItem == null)
                    {
                        return;
                    }
                }

                var rules = RuleFactory.GetRules<RuleContext>(rulesFolderItem, "Rule");
                if (rules == null || rules.Count == 0)
                {
                    return;
                }

                var ruleContext = new RuleContext
                {
                    Item = item
                };

                rules.Run(ruleContext);
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message, exception, typeof(RuleManager));
            }
        }
    }
}
