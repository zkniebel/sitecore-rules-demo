﻿using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Pipelines;
using Sitecore.Rules;

namespace Delphic.Platform.Sc.Rules
{
    /// <summary>
    /// The pipeline args rule context.
    /// </summary>
    /// <remarks>
    /// Credit to Jim "Jimbo" Baltika for developing this class
    /// </remarks>
    /// <typeparam name="TArgs">The type of the arguments.</typeparam>
    public abstract class PipelineArgsRuleContext<TArgs> : RuleContext, IPipelineArgsRuleContext<TArgs>
        where TArgs : PipelineArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineArgsRuleContext{TArgs}"/> class.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected PipelineArgsRuleContext([NotNull] TArgs args)
        {
            Assert.IsNotNull(args, "args");
            Args = args;
            Item = args.ProcessorItem.InnerItem;
        }

        /// <summary>
        /// Gets the args.
        /// </summary>
        [NotNull]
        public TArgs Args { get; }
    }
}
