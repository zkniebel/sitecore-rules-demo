﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Events;
using Sitecore.Publishing;

namespace Delphic.Platform.Sc.Rules.EventHandlers
{
    /// <summary>
    /// Handler for all item publishing events
    /// </summary>
    /// <remarks>
    /// Credit to Jim "Jimbo" Baltika for developing this class
    /// </remarks>
    public sealed class ItemPublishEventHandler
    {
        /* ReSharper disable NotNullMemberIsNotInitialized */

        /// <summary>
        /// Gets or sets the rule folder id.
        /// </summary>
        [NotNull]
        [UsedImplicitly]
        public string RuleFolderId { get; set; }

        /* ReSharper restore NotNullMemberIsNotInitialized */

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        private void RunRules([NotNull] object sender, [NotNull] EventArgs args)
        {
            Assert.ArgumentNotNull(sender, nameof(sender));
            Assert.ArgumentNotNull(args, nameof(args));

            if (string.IsNullOrWhiteSpace(this.RuleFolderId))
            {
                return;
            }

            var publisher = Event.ExtractParameter(args, 0) as Publisher;

            if (publisher != null)
            {
                // TODO: Implement call
            }
        }
    }
}
