﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Events;

namespace Delphic.Platform.Sc.Rules.EventHandlers
{
    /// <summary>
    /// Handler for all item events other than creation and publishing
    /// </summary>
    /// <remarks>
    /// Credit to Jim "Jimbo" Baltika for developing this class
    /// </remarks>
    public sealed class ItemEventHandler
    {
        /// <summary>
        /// Gets or sets the rule folder id.
        /// </summary>
        [UsedImplicitly]
        [CanBeNull]
        public string RuleFolderId { get; set; }

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The args.</param>
        private void RunRules([NotNull] object sender, [NotNull] EventArgs args)
        {
            Assert.ArgumentNotNull(sender, nameof(sender));
            Assert.ArgumentNotNull(args, nameof(args));

            if (string.IsNullOrWhiteSpace(RuleFolderId))
            {
                return;
            }

            var item = Event.ExtractParameter(args, 0) as Item;
            if (item == null)
            {
                return;
            }

            if (!Settings.Rules.ItemEventHandlers.RulesSupported(item.Database))
            {
                return;
            }

            ID id;
            if (ID.TryParse(RuleFolderId, out id))
            {
                RuleManager.RunRules(item, id);
            }
        }
    }
}
