﻿using System;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Diagnostics;
using Sitecore.Events;

namespace Delphic.Platform.Sc.Rules.EventHandlers
{
    /// <summary>
    /// Handler for all item creation events
    /// </summary>
    /// <remarks>
    /// Credit to Jim "Jimbo" Baltika for developing this class
    /// </remarks>
    public sealed class ItemCreatedEventHandler
    {
        /// <summary>
        /// Gets or sets the rule folder id.
        /// </summary>
        [UsedImplicitly]
        [CanBeNull]
        public string RuleFolderId { get; set; }

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The args.</param>
        private void RunRules([NotNull] object sender, [NotNull] EventArgs args)
        {
            Assert.ArgumentNotNull(sender, nameof(sender));
            Assert.ArgumentNotNull(args, nameof(args));

            if (string.IsNullOrWhiteSpace(RuleFolderId))
            {
                return;
            }

            var eventArgs = Event.ExtractParameter(args, 0) as ItemCreatedEventArgs;
            if (eventArgs == null)
            {
                return;
            }

            if (!Settings.Rules.ItemEventHandlers.RulesSupported(eventArgs.Item.Database))
            {
                return;
            }

            ID id;
            if (ID.TryParse(RuleFolderId, out id))
            {
                RuleManager.RunRules(eventArgs.Item, id);
            }
        }
    }
}
