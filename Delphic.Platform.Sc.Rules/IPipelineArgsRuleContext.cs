﻿using Sitecore;
using Sitecore.Data.Items;

namespace Delphic.Platform.Sc.Rules
{
    /// <summary>
    /// The PipelineArgsRuleContext interface.
    /// </summary>
    /// <remarks>
    /// Credit to Jim "Jimbo" Baltika for developing this class
    /// </remarks>
    /// <typeparam name="TArgs">The type of the arguments.</typeparam>
    public interface IPipelineArgsRuleContext<out TArgs>
        where TArgs : class
    {
        /// <summary>
        /// Gets the args.
        /// </summary>
        [NotNull]
        TArgs Args { get; }

        /// <summary>
        /// Gets the processor item.
        /// </summary>
        [NotNull]
        Item Item { get; }
    }
}
