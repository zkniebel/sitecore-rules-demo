﻿using System.Runtime.InteropServices;
using Delphic.Platform.Sc.Rules.Conditions.Base;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Managers;
using Sitecore.Diagnostics;
using Sitecore.Rules;

namespace Delphic.Platform.Sc.Rules.Conditions.ItemInformation
{
    /// <summary>
    /// Rule that walks the supplied Item's template inheritance hierarchy looking
    /// for a match to the TemplateID provided by the rule context.
    /// </summary>
    /// <typeparam name="TRuleContext">
    /// Instance of Sitecore.Rules.Conditions.RuleContext.
    /// </typeparam>
    [UsedImplicitly]
    [Guid("7C321F19-3E89-4EC1-B6E2-1E7C5D6ECF59")]
    public sealed class ItemInheritsFromTemplate<TRuleContext> : WhenCondition<TRuleContext>
        where TRuleContext : RuleContext
    {

        /// <summary>
        /// The backing field for the template Id.
        /// </summary>
        private ID templateId;

        /// <summary>
        /// Gets or sets the template id.
        /// </summary>
        [NotNull]
        public ID TemplateId
        {
            get
            {
                return templateId;
            }

            set
            {
                Assert.ArgumentNotNull(value, nameof(value));
                templateId = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemInheritsFromTemplate{TRuleContext}"/> class. 
        /// Initializes a new instance of the ItemInheritsFromTemplate class.
        /// </summary>
        public ItemInheritsFromTemplate()
        {
            templateId = ID.Null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemInheritsFromTemplate{TRuleContext}"/> class. 
        /// Initializes a new instance of the ItemInheritsFromTemplate class.
        /// </summary>
        /// <param name="templateId">
        /// The template id.
        /// </param>
        public ItemInheritsFromTemplate([NotNull] ID templateId)
        {
            Assert.ArgumentNotNull(templateId, nameof(templateId));
            this.templateId = templateId;
        }

        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        /// <returns>
        /// <c>True</c>, if the condition succeeds, otherwise <c>false</c>.
        /// </returns>
        protected override bool ExecuteRule(TRuleContext ruleContext)
        {
            var item = ruleContext.Item;
            if (item == null)
            {
                return false;
            }

            if (item.TemplateID == TemplateId)
            {
                return true;
            }

            var template = TemplateManager.GetTemplate(item);
            return template?.InheritsFrom(TemplateId) ?? false;
        }
    }
}
