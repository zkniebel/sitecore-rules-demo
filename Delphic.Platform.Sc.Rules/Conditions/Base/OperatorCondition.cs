﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore;
using Sitecore.Diagnostics;

namespace Delphic.Platform.Sc.Rules.Conditions.Base
{
    public abstract class OperatorCondition<TRuleContext> : Sitecore.Rules.Conditions.OperatorCondition<TRuleContext>
        where TRuleContext : Sitecore.Rules.RuleContext
    {
        /// <summary>
        /// Condition implementation.
        /// </summary>
        /// <remarks>
        /// Credit to Jim "Jimbo" Baltika for developing this class
        /// </remarks>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns>
        /// True if the item has layout details for the default device,
        /// otherwise False.
        /// </returns>
        protected override bool Execute([NotNull] TRuleContext ruleContext)
        {
            Assert.IsNotNull(ruleContext, "ruleContext");
            Assert.IsNotNull(ruleContext.Item, "ruleContext.Item");
            if (ruleContext.IsAborted)
            {
                return false;
            }

            try
            {
                Log.Debug("RuleAction " + GetType().Name + " started for " + ruleContext.Item.Name, this);

                var result = ExecuteRule(ruleContext);
                return result;
            }
            catch (Exception exception)
            {
                Log.Error("RuleAction " + GetType().Name + " failed.", exception, this);
                Trace.TraceError(exception.ToString());
            }

            Log.Debug("RuleAction " + GetType().Name + " ended for " + ruleContext.Item.Name, this);

            return false;
        }

        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns>
        /// <c>True</c>, if the condition succeeds, otherwise <c>false</c>.
        /// </returns>
        protected abstract bool ExecuteRule([NotNull] TRuleContext ruleContext);
    }
}
